A really simple web client for displaying a balance and transaction history for the Moneta bank. Uses its API available for all accounts.

I mainly want to use this for myself to avoid having the mobile banking app and still be able to see a quick transaction history. Also without extra fluff. :)
