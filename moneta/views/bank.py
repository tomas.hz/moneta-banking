import http.client
import json
import typing

import flask
import requests
import werkzeug.exceptions

__all__ = ["bank_blueprint"]

bank_blueprint = flask.Blueprint(
	"bank",
	__name__
)

@bank_blueprint.route("/")
def view_bank() -> typing.Union[flask.Response, int]:
	PRESET_HEADERS = {
		"Accept": "application/json",
		"Authorization": f"Bearer {flask.current_app.config['API_KEY']}"
	}
	
	accounts = requests.get(
		"https://api.moneta.cz/api/v3/vip/aisp/my/accounts?page=0&size=20", # Ignore >20 accounts
		headers=PRESET_HEADERS
	)

	if not accounts.ok:
		raise werkzeug.exceptions.InternalServerError

	accounts = accounts.json()["accounts"]

	formatted_accounts = {}

	for account in accounts:
		useful_identifier = (
			account
			["identification"]
			["other"]
		)
		
		formatted_accounts[useful_identifier] = {
			"plan": account["feePlanI18N"],
			"type": account["productI18N"],
			"balance": 0,
			"transaction_history": []
		}
		
		## Balance
		
		balances = requests.get(
			f"https://api.moneta.cz/api/v3/vip/aisp/my/accounts/{account['id']}/balance",
			headers=PRESET_HEADERS
		)
		
		if not balances.ok:
			raise werkzeug.exceptions.InternalServerError
		
		balances = balances.json()["balances"]
		
		formatted_accounts[useful_identifier]["balance"] = {
			"amount": balances[0]["amount"]["value"],
			"currency": balances[0]["amount"]["currency"]
		}
		
		## Transaction history
		
		transaction_history = requests.get(
			f"https://api.moneta.cz/api/v3/vip/aisp/my/accounts/{account['id']}/transactions?page=0&size=20",
			headers=PRESET_HEADERS
		)
		
		if not transaction_history.ok:
			raise werkzeug.exceptions.InternalServerError
		
		transaction_history = transaction_history.json()["transactions"]
		
		for transaction in transaction_history:
			formatted_accounts[useful_identifier]["transaction_history"].append({
				"money": {
					"amount": transaction["amount"]["value"],
					"currency": transaction["amount"]["currency"]
				},
				"received": (transaction["creditDebitIndicator"] == "CRDT"),
				"date": transaction["enteredDate"]["date"],
				"type": (
					transaction
					["entryDetails"]
					["transactionDetails"]
					.get("additionalTransactionInformation", "")
				),
				"issuer": (
					transaction
					["entryDetails"]
					["transactionDetails"]
					["references"]
					.get("transactionDescription", "")
				)
			})

	return flask.render_template(
		"view_bank.html",
		accounts=formatted_accounts
	), http.client.OK
